module.exports = {
    name: 'prune',
    description: 'prunes the chat',
    args: true,
    syntax: '-prune [number]',
    guildOnly: true,
    execute(message, args) {
        const amount = parseInt(args[0]) + 1;

        if (isNaN(amount)) {
            return message.reply('Keine gültige Nummer');
        } else if (amount <= 1 || amount > 50) {
            return message.reply('nur Nummern zwischen 1 und 50');
        }
        message.channel.bulkDelete(amount, true).catch(err => {
            console.error(err);
            message.channel.send('Error pruning the channel');
        });
        message.channel.send(`${message.author.username} pruned ${amount - 1} messages.`);
    }
}
