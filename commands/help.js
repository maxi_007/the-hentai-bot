const {prefix} = require('../config.json');
module.exports = {
    name: 'help',
    description: 'help command',
    aliases: ['commands'],
    syntax: '-help (command name)',
    cooldown: 5,
    execute(message, args) {
        const data = [];
        const {commands} = message.client;

        if (!args.length) {
            data.push('List of all commands:');
            data.push(commands.map(command => command.name).join(', '));
            data.push(`\nYou can send \`${prefix}help [command name]\` to get info on a specific command.`);

            return message.author.send(data, {split: true})
                .then(() => {
                    if (message.channel.type === 'dm') return;
                    message.reply('I\'ve sent you an DM with all my commands.');
                })
                .catch(error => {
                    console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
                    message.reply('It seems like I can\'t DM you!) Do you have DMs disabled?');
                });
        }
    }
}