module.exports = {
    name: 'server-info',
    description: 'Server Info',
    guildOnly: true,
    aliases: ['serverinfo','server'],
    cooldown: 10,
    execute(message, args) {
        message.channel.send(`Servername: ${message.guild.name}\nNumber of Members: ${message.guild.memberCount}`);
    }
}