module.exports = {
    name: 'avatar',
    description: 'Avatar Link output',
    args: true,
    syntax: '-avatar user1 user2 ..',
    guildOnly: true,
    aliases: ['icon','pfp'],
    execute(message,args) {
        const avatarList = message.mentions.users.map(user => {
            return `${user.username}'s Avatar: ${user.displayAvatarURL()}`;
        })
        message.channel.send(avatarList);
    }
}