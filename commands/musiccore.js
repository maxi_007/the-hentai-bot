const {prefix} = require('../config.json');
const ytdl = require('ytdl-core');
const {OpusEncoder} = require('@discordjs/opus');
const discordBot = require('../hentaibotcore');

module.exports = {
    name: 'musiccore',
    description: 'core of the Music-bot',
    args: false,
    guildOnly: true,
    aliases: ['play', 'skip', 'stop'],
    cooldown: 0.1,
    async execute(message, args) {


        const voiceChannel = message.member.voice.channel;
        //const queueCommand = queue;
        //const queue = [];

        if (!voiceChannel) {
            return message.reply('join voicechannel pls');
        }

        if (message.content.startsWith(`${prefix}play`)) {

            if (!args[0]) {
                return message.reply('No Arguments!');
            }
            if (!discordBot.queue[0]) {
                discordBot.queue.push(args[0]);
                await this.playX(voiceChannel);
            } else if (discordBot.queue[0]) {
                discordBot.queue.push(args[0]);
                message.reply(`The Song has been added to the queue`);
            }
        } else if (message.content.startsWith(`${prefix}skip`)) {
            if (!discordBot.queue[1]) {
                return message.reply('There is no Song to skip to!');
            }
            message.reply('Skipping current Song..');
            discordBot.queue.shift();
            this.playX(voiceChannel);

        } else if (message.content.startsWith(`${prefix}stop`)) {
            voiceChannel.leave();
            discordBot.queue = [];
            //message.reply('stop response');
            console.info(message.content);
        } else {
            message.reply('invalid command');
        }
    },
    playX(voiceChannel) {
        voiceChannel.join().then(connection => {
            const stream = ytdl(discordBot.queue[0], {filter: 'audioonly'});
            const dispatcher = connection.play(stream);

            dispatcher.on('finish', () => {
                if (discordBot.queue[1]) {
                    discordBot.queue.shift();
                    this.playX(voiceChannel);
                } else if (!discordBot.queue[1]) {
                    voiceChannel.leave();
                    discordBot.queue = [];
                }
            });
        })
    }
}