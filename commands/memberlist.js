module.exports = {
    name: 'memberlist',
    description: 'creates .json file with serverInfo',
    syntax: ' -memberlist filename',
    args: true,
    guildOnly: true,
    async execute(message, args) {
        const fs = require('fs');
        const fileName = args[0] + ".json";
        console.warn(fileName);
        const memberList = await message.guild.members.fetch();
        console.warn(JSON.stringify(memberList));
        const memberListJSON = JSON.stringify(memberList);
        fs.writeFileSync("./json-files/" + fileName, memberListJSON);

        message.reply(`The JSON file has been saved in ./json-files/${fileName}`);
    }
}