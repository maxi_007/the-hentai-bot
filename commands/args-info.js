module.exports = {
    name: 'args-info',
    cooldown: 5,
    description: 'arguments-info',
    args: true,
    syntax: '-args-info argument1 argument2 ..',
    guildOnly: false,
    execute(message, args) {
        if (args[0] === 'test') {
            return message.channel.send('test respond');
        }

        message.channel.send(`Command name: ${this.name}\nArguments: ${args}`);
    }
}