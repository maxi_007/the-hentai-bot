//Nodes native file System
const fs = require('fs');
//Discord.js Library
const Discord = require('discord.js');
// Read config for prefix and bot-token
const {prefix, token} = require('./config.json');

// main hub for interacting with the Discord API
const client = new Discord.Client();
// creates new Collection (aka. Map)
client.commands = new Discord.Collection();

module.exports = { queue : []}

// Uses Node native fs that addresses to the directory './commands'
// which then filters the whole content of the directory out and uses only .js files
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

//this for-loop cycles through all .js files and initializes them properly.
//it also uses the 'name' string in the .js file to initialize the actual command
//that will later be used for incoming commands via chat-message
for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

// creates a new Map for cooldown purposes (later use)
const cooldowns = new Discord.Collection();

//waits for ready event from the bot and makes an output into the console after the bot initialized everything properly

client.once('ready', () => {
    console.log('Ready!');
});

// The bot now logs into the application via token that is addressed in the config.json
client.login(token);

// listener of messages
client.on('message', message => {
    // looks if a message contains not the prefix or if the message comes from a bot, stops immediately to try to run something since no prefix is provided.
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    // a constant called args is created which splits away the prefix from the message
    const args = message.content.slice(prefix.length).split(/ +/);
    // the command that is provided is reformatted to only lowercase
    const commandName = args.shift().toLowerCase();

    // a constant that contains the current time
    const currentTime = new Date(Date.now());
    // converts the currentTime constant to a readable string value
    currentTime.toString();
    // Console outputs about the sended message (not important but good for spying or some other shit lmao)
    console.log(currentTime);
    console.log(`message content: "${message.content}"`);
    console.log(message.author.username);
    console.log(message.attachments);

    // looks if the message that the bot received is from a Server-Text-Channel or from a PM, and gives out the Servername
    if (message.channel.type === 'text') {
        console.log(`From Server: ${message.guild.name}`);
    }

    //TODO Fertig kommentieren lol
    const command = client.commands.get(commandName) ||
        client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) return;

    if (command.guildOnly && message.channel.type !== 'text') {
        return message.reply('Can not run that command in DMs ');
    }

    if (command.args && !args.length) {
        if (command.syntax) {
            return message.channel.send
            (`No Arguments ${message.author}!\nThe Syntax is:${command.syntax}`);
        } else {
            return message.channel.send
            (`No Arguments ${message.author}!\nThere is no Syntax given.`);
        }
    }

    if (!cooldowns.has(command.name)) {
        cooldowns.set(command.name, new Discord.Collection());
    }

    const now = Date.now();
    const timestamps = cooldowns.get(command.name);
    const cooldownAmount = (command.cooldown || 3) * 1000;

    if (timestamps.has(message.author.id)) {
        const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

        if (now < expirationTime) {
            const timeLeft = (expirationTime - now) / 1000;
            return message.reply(`Please wait ${timeLeft.toFixed(1)} 
            Seconds until you can use ${command.name} again.`);
        }
    }

    timestamps.set(message.author.id, now);
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

    try {
        command.execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('HOLY F*CKING SHIT, SOMETHING BAD HAPPENED EXECUTING YOUR COMMAND!! DEVELOPER!! QUICK, HAVE A LOOK INTO THE CONSOLE!!');
        message.reply(error);
    }
});
























